import React, { Component } from "react";
import Navbar from "./Navbar";
import NewPlayer from "./NewPlayer";

export default class Crud extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <div className="container mt-4">
          <NewPlayer />
        </div>
      </div>
    );
  }
}
